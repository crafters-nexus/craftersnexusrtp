package com.craftersnexus.craftersnexusrtp;

import com.wimbli.WorldBorder.BorderData;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.ThreadLocalRandom;

public class RandomTeleporter extends BukkitRunnable {
    private final BorderData border;
    private final World world;
    private final Player player;
    private final Location playerLoc;
    private final double minX;
    private final double maxX;
    private final double minZ;
    private final double maxZ;
    private int attempts = 0;
    private final int maxTries = ConfigHandler.getMaxTries();

    public RandomTeleporter(BorderData border, double radius, World world, Player player, boolean aroundPlayerMode) {
        this.border = border;
        this.world = world;
        this.player = player;
        this.playerLoc = this.player.getLocation();

        // Set min and max values for search
        if(aroundPlayerMode) {
            double playerX = player.getLocation().getX();
            double playerZ = player.getLocation().getZ();
            this.minX = playerX - radius;
            this.maxX = playerX + radius;
            this.minZ = playerZ - radius;
            this.maxZ = playerZ + radius;

        } else {
            double borderX;
            double borderZ;
            if (border == null) {
                borderX = world.getSpawnLocation().getX();
                borderZ = world.getSpawnLocation().getZ();
            } else {
                borderX = border.getX();
                borderZ = border.getZ();
            }
            this.minX = borderX - radius;
            this.maxX = borderX + radius;
            this.minZ = borderZ - radius;
            this.maxZ = borderZ + radius;
        }
    }

    private boolean isInsideBorder(Location loc) {
        if(border == null) {
            return true;
        } else {
            return border.insideBorder(loc);
        }
    }

    @Override
    public void run() {
        if(attempts > maxTries) {
            player.sendMessage(ChatColor.RED + "Unable to find a safe location to teleport to.");
            this.cancel();
        }
        double x = ThreadLocalRandom.current().nextInt((int) minX, (int) maxX);
        double z = ThreadLocalRandom.current().nextInt((int) minZ, (int) maxZ);
        double y = world.getHighestBlockYAt((int) x, (int) z);
        Location newLoc = new Location(world, x + 0.5, y, z + 0.5);
        Block newBlock = newLoc.getBlock();
        // Get Block under player
        Block underPlayer = newBlock.getRelative(0, -1, 0);
        // Get block above player
        Block topBlock = newBlock.getRelative(0,1,0);
        if(isInsideBorder(newLoc)) {
            if (newBlock.isEmpty()) {
                if (topBlock.isEmpty()) {
                    if(!underPlayer.isLiquid()) {
                        if (!underPlayer.isEmpty()) {
                            if (!newLoc.equals(playerLoc)) {
                                if (!underPlayer.getType().equals(Material.BEDROCK)) {
//                                    player.teleport(newLoc);
//                                    player.sendMessage(ChatColor.GREEN + "You have been randomly teleported to " + x + ", " + y + ", " + z);
                                    // Cancel async task and start a delayed TP outside of the task
                                    this.cancel();
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            player.teleport(newLoc);
                                            player.sendMessage(ChatColor.GREEN + "You have been randomly teleported to " + x + ", " + y + ", " + z);
                                        }
                                    }.runTaskLater(CraftersNexusRTP.getPlugin(CraftersNexusRTP.class), 20);
                                }
                            }
                        }
                    }
                }
            }
        }
        attempts++;
    }
}
