package com.craftersnexus.craftersnexusrtp.commands;

import com.craftersnexus.craftersnexusrtp.ConfigHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RTPSubCommand {

    // The RTP remove command
    public boolean rtpRemoveCommand(CommandSender commandSender, String[] args) {
        if (commandSender.hasPermission("craftersnexus.rtp.remove")) {
            String worldName;
            // The have a second arugment so it might be a world to add
            if (args.length == 2) {
                World worldTest = Bukkit.getWorld(args[1]);
                if (worldTest == null) {
                    commandSender.sendMessage(ChatColor.RED + "The specified world: " + args[1] + " does not exist.");
                    return true;
                } else {
                    worldName = args[1].toLowerCase();
                }
            }
            // No second argument so just get the world from the player
            else {
                if (commandSender instanceof Player) {
                    Player player = (Player) commandSender;
                    // Get the world name from the player instead
                    worldName = player.getWorld().getName().toLowerCase();
                } else {
                    commandSender.sendMessage("You must be a player to use this command without specifying a world name!");
                    return true;
                }
            }
            ConfigHandler.removeRtpWorld(worldName);
            commandSender.sendMessage(ChatColor.GREEN + "Removed world: " + worldName + " from the RTP list.");
        } else {
            commandSender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
        }
        return true;
    }

    // The RTP add command
    public boolean rtpAddCommand(CommandSender commandSender, String[] args) {
        if (commandSender.hasPermission("craftersnexus.rtp.add")) {
            String worldName;
            // The have a second arugment so it might be a world to add
            if (args.length == 2) {
                World worldTest = Bukkit.getWorld(args[1]);
                if (worldTest == null) {
                    commandSender.sendMessage(ChatColor.RED + "The specified world: " + args[1] + " does not exist.");
                    return true;
                } else {
                    worldName = args[1].toLowerCase();
                }
            }
            // No second argument so just get the world from the player if it's an actual player
            else {
                if (commandSender instanceof Player) {
                    Player player = (Player) commandSender;
                    // Get the world name from the player instead
                    worldName = player.getWorld().getName().toLowerCase();
                } else {
                    commandSender.sendMessage("You must be a player to use this command without specifying a world name!");
                    return true;
                }
            }
            ConfigHandler.addRtpWorld(worldName);
            commandSender.sendMessage(ChatColor.GREEN + "Added world: " + worldName + " to the RTP list.");
        } else {
            commandSender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
        }
        return true;
    }

    // RTP Others command
    public boolean rtpOthersCommand(CommandSender commandSender, String[] args) {
        if (!commandSender.hasPermission("craftersnexus.rtp.others")) {
            commandSender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
            return true;
        }

        // Get the player name from the command args and the world name
        String playerName = args[0];
        String worldName = args[1].toLowerCase();
        // Get the target player from the server by their name.
        @SuppressWarnings("deprecation")
        Player targetPlayer = Bukkit.getServer().getPlayer(playerName);

        // If the target player or the world is null, tell the command sender
        if (targetPlayer == null) {
            commandSender.sendMessage(ChatColor.RED + "A player by the name: " + playerName + " was not found.");
            return true;
        }

        World world = Bukkit.getServer().getWorld(worldName);
        if (world == null) {
            commandSender.sendMessage(ChatColor.RED + "The world: " + worldName + " Does not exist.");
            return true;
        } else {
            if (ConfigHandler.worldNotEnabled(worldName)) {
                commandSender.sendMessage(ChatColor.RED + "RTP in this world is not enabled!");
                return true;
            }
            // All checks passed, do random teleport!
            return RTPCommand.doRandomTeleport(targetPlayer, world);
        }
    }
}
